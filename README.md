
# Treehouse - Address Book #

This repository contains notes and practice examples from the course
**Build an Address Book in Ruby**, imparted by Jason Seifer at [Threehouse][JF].

> In this course, you'll build a simple command line address book application
> using Ruby. You'll put to use a lot of skills learned in previous courses to
> put everything together: objects, classes, blocks, input and output, and more.

## Contents ##

- **Class Design**: In this stage we're going to start to design the classes
  that we'll use throughout the rest of the address book program.
- **Search**: Search is an integral part of any address book application. In
  this stage, you'll learn how to add the ability to search to your classes.
- **Input and Output**: Our program functionality is all set up but our users
  have no way to interact with the program. In this stage we add a UI to the
  application and add a text based menu and interaction system.

## Extra ##

* The ability to delete contacts from the address book.
* Interact with the address and phone numbers in the contact class.

---
This repository contains code examples from Treehouse. These are included under
fair use for showcasing purposes only. Those examples may have been modified to
fit my particular coding style.

[JF]: http://teamtreehouse.com
