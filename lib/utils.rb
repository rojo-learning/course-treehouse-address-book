
module Utils
  def require_input(text)
    print "#{text} "
    gets.chomp
  end

  def titleize(attribute)
    attribute.to_s.split('_').map(&:capitalize!).join(' ')
  end

  def bulk_edit(attributes)
    attributes.each do |attribute|
      puts "#{titleize attribute} has the value: #{eval "@#{attribute}"}"
      value = require_input('New value («Intro» to leave unchanged):')
      self.send("#{attribute}=", value) unless value.empty?
    end
  end

  def manage_element(elements, index, &block)
    element = elements.at(index)

    if element.nil?
      puts "No entry with index #{index} was found."
    else
      block.call(element, index) if block_given?
    end
  end

  def print_elements(elements_reader, numbered: false)
    title = titleize(elements_reader)
    elements = self.send(elements_reader)

    puts "#{title}"
    if elements.empty?
      puts "» No #{title.downcase}"
    else
      if numbered
        elements.each_with_index do |element, index|
          puts "#{index}.- #{element}"
        end
      else
        elements.each { |element| puts element }
      end
    end
  end
end
