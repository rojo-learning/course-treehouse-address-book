
require_relative 'contact'

class AddressBook
  attr_reader :contacts

  def initialize
    @contacts = []
  end

  include Utils

  def add_contact(first_name:'', middle_name:'', last_name:'', require: false)
    if require
      first_name  = require_input('First name:' )
      middle_name = require_input('Middle name:')
      last_name   = require_input('Last name:'  )
    end

    Contact.new(
      first_name:  first_name,
      middle_name: middle_name,
      last_name:   last_name
    ).tap { |contact| contacts.push contact }
  end

  def select_contact(index)
    contacts.at index
  end

  def delete_contact(contact)
    contacts.delete_if { |entry| entry.to_s.eql? contact.to_s }
  end

  def add_contact_phone(contact)
    contact.add_phone_number require: true
  end

  def add_contact_address(contact)
    contact.add_address require: true
  end

  def find_by(attribute, query, print: false)
    [].tap do |results|
      results = contacts.select do |contact|
        contact.send "has_#{attribute}_with?", query
      end

      print_search_results(titleize(attribute), query, results) if print
    end
  end

  def find_by_name(query, print: false)
    find_by :name, query, print: print
  end

  def find_by_phone(query, print: false)
    find_by :phone, query, print: print
  end

  def find_by_address(query, print: false)
    find_by :address, query, print: print
  end

  def print_contact_list(numbered: false)
    print_elements :contacts, numbered: numbered
  end

  def print_search_results(topic, search, results)
    puts "#{topic} search results for: #{search}"
    unless results.empty?
      results.each do |contact|
        puts "-" * 50
        puts contact
        contact.print_phone_numbers
        contact.print_addresses
        puts "-" * 50
      end
    else
      puts "No contacts found."
    end
  end
end
