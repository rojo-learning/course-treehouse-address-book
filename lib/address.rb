
require_relative 'utils'

class Address
  attr_accessor :kind, :street_1, :street_2, :city, :state, :postal_code

  include Utils

  def initialize(kind:, street_1:, street_2: '', city:, state:, postal_code:)
    @kind        = kind
    @street_1    = street_1
    @street_2    = street_2
    @city        = city
    @state       = state
    @postal_code = postal_code
  end

  def edit
    bulk_edit [:kind, :street_1, :street_2, :city, :state, :postal_code]
  end

  def long_format
    address  = "#{street_1}\n"
    address << "#{street_2}\n" unless street_2.empty?
    address << "#{city}, #{state} #{postal_code}"
  end

  def short_format
    address  = "#{kind}: #{street_1}"
    address << " #{street_2}" unless street_2.empty?
    address << ", #{city}, #{state}, #{postal_code}"
  end

  def to_s(format='short')
    case format
    when 'long'  then long_format
    when 'short' then short_format
    else
      short_format
    end
  end

  def self.to_s
    'address'
  end
end
