
require_relative 'utils'

class PhoneNumber
  class << self
    def to_s
      'phone number'
    end
  end

  attr_accessor :kind, :phone
  alias_method :number, :phone

  include Utils

  def initialize(kind, phone)
    @kind  = kind
    @phone = phone
  end

  def area_code
    @phone[0,3]
  end

  def edit
    bulk_edit [:kind, :phone]
  end

  def to_s
    "#{kind}: (#{area_code}) #{@phone[3,3]}-#{@phone[6,4]}"
  end
end
