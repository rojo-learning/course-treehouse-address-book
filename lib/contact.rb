
require_relative 'address'
require_relative 'phone_number'

class Contact
  attr_accessor :first_name, :middle_name, :last_name
  attr_reader   :phone_numbers, :addresses

  include Utils

  def initialize(first_name:, middle_name:'', last_name:)
    @first_name  = first_name
    @last_name   = last_name
    @middle_name = middle_name
    @phone_numbers = []
    @addresses     = []
  end

  def full_name
    [first_name, middle_name, last_name].reject{ |e| e.empty? }.join(' ')
  end

  def first_last
    first_name + ' ' + last_name
  end

  def last_first
    last_first  = last_name + ', ' + first_name
    last_first += middle_name.empty? ? '' : " #{middle_name[0]}."
  end

  def has_name_with?(query)
    full_name.downcase.include? query.downcase
  end

  def edit_name
    bulk_edit [:first_name, :middle_name, :last_name]
  end

  def add_phone_number(kind:'', number:'', require: false)
    if require
      kind   = require_input('Kind (Mobile, Work, etc):')
      number = require_input('Number:')
    end

    PhoneNumber.new(kind, number).tap { |phone| phone_numbers.push phone }
  end

  def edit_phone_number(index)
    manage_element(phone_numbers, index) { |phone, _| phone.edit }
  end

  def delete_phone_number(index)
    manage_element(phone_numbers, index) { |_, index| phone_numbers.delete_at index }
  end

  def has_phone_with?(query)
    phone_numbers.any? do |phone_number|
      phone_number.number.gsub('-', '').include? query
    end
  end

  def print_phone_numbers(numbered: false)
    print_elements :phone_numbers, numbered: numbered
  end

  def add_address(kind:'', street_1:'', street_2:'', city:'', state:'', postal_code:'', require: false)
    if require
      kind     = require_input('Kind (Home, Work, etc):')
      street_1 = require_input('Street:')
      street_2 = require_input('Street:')
      city     = require_input('City:')
      state    = require_input('State:')
      postal_code = require_input('Postal Code:')
    end

    Address.new(
      kind: kind,
      street_1: street_1,
      street_2: street_2,
      city: city,
      state: state,
      postal_code: postal_code
    ).tap { |address| addresses.push address }
  end

  def edit_address(index)
    manage_element(addresses, index) { |address, _| address.edit }
  end

  def delete_address(index)
    manage_element(addresses, index) { |_, index| addresses.delete_at index }
  end

  def has_address_with?(query)
    addresses.any? { |address| address.to_s.downcase.include? query.downcase }
  end

  def print_addresses(numbered: false)
    print_elements :addresses, numbered: numbered
  end

  def to_s(format = 'last_first')
    case format
    when 'full_name'  then full_name
    when 'last_first' then last_first
    when 'first'      then first
    when 'last'       then last
    else
      last_first
    end
  end
end
