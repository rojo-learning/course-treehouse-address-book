
require 'yaml'

require_relative 'lib/address_book'

class Main
  attr_reader :address_book

  include Utils

  CONTACTS_FILE = './data/contacts.yml'

  def initialize
    @address_book = AddressBook.new
  end

  def open
    if File.exists?(CONTACTS_FILE)
      YAML.load_file(CONTACTS_FILE).each do |contact|
        address_book.contacts.push contact
      end
    end
  end

  def run
    open

    loop do
      puts
      puts  'Address Book'
      puts  'e: Exit'
      puts  'a: Add Contact'
      puts  'm: Manage Contact'
      puts  'p: Print Address Book'
      puts  's: Search'

      case require_input('Enter your choice:')
      when 'e' then save and break
      when 'a' then add_contact
      when 'm' then manage_contact
      when 'p' then print_contacts
      when 's' then search_contacts
      end
    end
  end

  def save
    File.open(CONTACTS_FILE, 'w') do |file|
      file.write address_book.contacts.to_yaml
    end
  end

  def search_contacts
    search = require_input('Search term:')
    address_book.find_by_name search, print: true
    address_book.find_by_phone search, print: true
    address_book.find_by_address search, print: true
  end

  def print_contacts
    puts
    address_book.print_contact_list numbered: true
  end

  def add_contact
    contact = address_book.add_contact(require: true)

    loop do
      puts  'Add phone number or address?'
      puts  'p: Add Phone Number'
      puts  'a: Add Address'
      puts  '(Any other key to go back)'

      case require_input('Enter your choice:')
      when 'a' then add_contact_address(contact)
      when 'p' then add_contact_phone_number(contact)
      else
        break
      end
    end
  end

  def add_contact_address(contact)
    contact.add_address require: true
  end

  def add_contact_phone_number(contact)
    contact.add_phone_number require: true
  end

  def manage_contact
    index   = require_input('Type the contact index number:').to_i
    contact = address_book.select_contact(index)

    if contact.nil?
      puts "No contact with the index #{index} was found."
    else
      loop do
        puts
        puts " » » » » #{contact}"
        puts 'p: Print Contact Information'
        puts 'a: Manage Addresses'
        puts 'n: Manage Phone Numbers'
        puts 'e: Edit Name'
        puts 'd: Delete Contact'
        puts 'g: Go Back'

        case require_input('Enter your choice:')
        when 'p' then print_contact_info(contact)
        when 'a' then manage_contact_addresses(contact)
        when 'n' then manage_contact_phones(contact)
        when 'e' then edit_contact_name(contact)
        when 'd' then delete_contact(contact); break
        when 'g' then break
        end
      end
    end
  end

  def print_contact_info(contact)
    puts
    puts "Name: #{contact}"
    contact.print_phone_numbers
    contact.print_addresses
  end

  def edit_contact_name(contact)
    contact.edit_name
  end

  def delete_contact(contact)
    puts "The following contact will be deleted:"
    print_contact_info contact

    if require_input('Do you want to proceed? (Yes/No)') =~ /\A(y|yes)\z/i
      address_book.delete_contact contact
      puts "Contact deleted!"
    end
  end

  def manage_contact_addresses(contact)
    loop do
      puts
      puts " » » » » #{contact}: Managing Addresses"
      puts 'What would your like to do?'
      puts 'p: Print Addresses'
      puts 'a: Add Address'
      puts 'e: Edit Address'
      puts 'd: Delete Address'
      puts 'g: Go Back'

      case require_input('What would you like to do?')
      when 'p' then print_contact_addresses(contact)
      when 'a' then add_contact_address(contact)
      when 'e' then edit_contact_address(contact)
      when 'd' then delete_contact_address(contact)
      when 'g' then break
      end
    end
  end

  def edit_contact_address(contact)
    contact.edit_address require_input('Type the address index number:').to_i
  end

  def delete_contact_address(contact)
    contact.delete_address require_input('Type the address index number:').to_i
  end

  def print_contact_addresses(contact)
    contact.print_addresses numbered: true
  end

  def manage_contact_phones(contact)
    loop do
      puts
      puts " » » » » #{contact}: Managing Phone Numbers"
      puts 'What would your like to do?'
      puts 'p: Print Phone Numbers'
      puts 'a: Add Phone Number'
      puts 'e: Edit Phone Number'
      puts 'd: Delete Phone Number'
      puts 'g: Go Back'

      case require_input('What would you like to do?')
      when 'p' then print_contact_phones(contact)
      when 'a' then add_contact_phone_number(contact)
      when 'e' then edit_contact_phone(contact)
      when 'd' then delete_contact_phone(contact)
      when 'g' then break
      end
    end
  end

  def edit_contact_phone(contact)
    contact.edit_phone_number require_input('Type the address index number:').to_i
  end

  def delete_contact_phone(contact)
    contact.delete_phone_number require_input('Type the address index number:').to_i
  end

  def print_contact_phones(contact)
    contact.print_phone_numbers numbered: true
  end
end

Main.new.run
